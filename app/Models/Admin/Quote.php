<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Quote extends Model
{
    protected  $table="quotes";
    protected $fillable=['id','title_vi','title_en','user_id','status'];
    protected $perPage = 8;
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public  function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y - H:i:s');
    }
    public function getTitleAttribute(){
        if(LaravelLocalization::setLocale()=='vi'){
            return $this->attributes['title_vi'];
        }elseif(LaravelLocalization::setLocale()=='en'){
            return $this->attributes['title_en'];
        }else{
            return false;
        }
    }
}
