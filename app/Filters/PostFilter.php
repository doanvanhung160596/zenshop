<?php
namespace App\Filters;
use Illuminate\Http\Request;
use App\User;
class PostFilter extends Filters
{
    protected $filters = ['search'];
    /**
     * @param $username
     * @return mixed
     */
 // ví dụ ở lọc kia có 1 key là min thì thêm 1 key ở $filter xong viết 1 function tên min
//    public function search($category)
//    {
//        return $this->builder->whereHas('category', function($q) use($category){
//            $q->where('slug', $category);
//        });
//    }

    public function search($value)
    {
        return $this->builder
            ->where('title','like',"%$value%")
            ->where('title_seal','like',"%$value%");
    }

//    public function min($min)
//    {
//        return $this->builder
//            ->where('viewer', '>=', $min);
//    }

}