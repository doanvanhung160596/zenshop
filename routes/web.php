<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Str;

Route::group(['prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect']], function()
{

    //DISPLAY HOME
    Route::get('/','Display\Home\HomeController@index');
    Route::get('home.html','Display\Home\HomeController@index')->name('home');
    //DISPLAY ACOUNT
    Route::get('login.html','Auth\LoginController@showLoginForm')->name('login.account');
    Route::post('login','Auth\LoginController@login')->name('login');
    Route::post('logout','Auth\LoginController@logout')->name('logout');
    Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset.html','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/reset','Auth\ResetPasswordController@reset')->name('request.password');
    Route::get('password/reset/{token}/{email}.html','Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::get('register.html','Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register','Auth\RegisterController@register')->name('register.account');
    Route::get('information/account','Auth\InformationController@account')->name('information.account');
    Route::post('information/update/{user}','Auth\InformationController@update')->name('information.update');
    Route::view('information/update','auth.information_success')->name('information.update.success');

    //Route::get('information/edit','Auth\InformationController@edit')->name('information.edit');
//DISPLAY Hello Mail Customer
//Route::get('verifie/{code}', 'Auth\RegisterController@verifie')->name('mail.verifie');

//DISPLAY PAGE
    Route::get('trang/{slug}','Display\Page\PageController@show')->name('page');
//DISPLAY POST
    Route::get('bai-viet/{category}','Display\Post\PostController@category')->name('post.category');
    Route::get('bai-viet/{category}/{slug}','Display\Post\PostController@index')->name('post.display');
    Route::get('bai-viet/{category}/{slug}/{parent_slug}','Display\Post\PostController@show')->name('post.display.show')->middleware('view.post.count');
//DISPLAY PRODUCT
    Route::get('san-pham/search','Display\Product\ProductController@search')->name('product.search');
    Route::get('san-pham/{category}','Display\Product\ProductController@category')->name('product.category');
    Route::get('san-pham/{category}/{slug}','Display\Product\ProductController@index')->name('product.display');
    Route::get('san-pham/{category}/{slug}/{parent_slug}','Display\Product\ProductController@show')->name('product.display.show')->middleware('view.count');

//DISPLAY CART
    Route::get('gio-hang/add','Display\Cart\CartController@add')->name('cart.add');
    Route::post('gio-hang/store','Display\Cart\CartController@store')->name('cart.store');
    Route::get('gio-hang/detail','Display\Cart\CartController@index')->name('cart.detail');
    Route::get('gio-hang/content_cart','Display\Cart\CartController@content_cart')->name('cart.content_cart');
    Route::get('gio-hang/change_cart','Display\Cart\CartController@change_cart')->name('cart.change_cart');
    Route::post('gio-hang/delete','Display\Cart\CartController@delete')->name('cart.delete');
    Route::get('gio-hang/delivery','Display\Cart\CartController@delivery')->name('cart.delivery');
    Route::post('gio-hang/delivery/get_city','Display\Cart\CartController@get_city')->name('cart.delivery.get_city');
    Route::post('gio-hang/delivery','Display\Cart\CartController@delivery')->name('cart.delivery.change');
    Route::post('gio-hang/add/delivery','Display\Cart\CartController@add_delivery')->name('cart.delivery.add');
    Route::post('gio-hang/update','Display\Cart\CartController@update_qty')->name('cart.update');
    Route::get('gio-hang/confirm','Display\Cart\CartController@confirm')->name('cart.confirm');
    Route::post('gio-hang/confirm/success','Display\Cart\CartController@confirm_success')->name('cart.confirm_success');
    Route::view('gio-hang/confirm/success','display.cart.success_cart')->name('cart.confirm_sc');
    //Add email customer
    Route::post('email/customer/store','Display\Email\Email_customerController@store')->name('email.customer.store')->middleware('throttle:2,1');
    //Search Category -Product
    Route::get('search','Display\Home\HomeController@search')->name('search');
    //Search Category -Post
    Route::get('search/post','Display\Post\PostController@search')->name('search.post');

    Route::get('/email/success',function (){
        return view('auth.passwords.reset_success');
    });

    Route::get('ok',function (){
        cache()->flush();
        return $cache = cache('recent_post');
//        return collect($cache)->take(2);
//        return $cart=Cart::content();
        $chuoi = '1-2-3-4-5-6-7-8-9-10-11-12';
        $kq = '1-2-3-4-5-6, 7-8-9-10-11-12';
        $array = explode('-', $chuoi);
        return collect($array)->chunk(6)->map(function($item){
            return $item->implode('-');
        })->implode(',');
        return App\User::all()->map(function($user){
            return
                ['email' => $user->email,
                'image' => $user->image
            ];
        });
    });
    Route::get('testproduct', function(){
//        $product= new \App\Models\Admin\Product();
//        $p1 = $product->where('id', 2)->first();
        return $p2 = \App\Models\Admin\Product::where(['id' => 2])->first();

        dd($p1, $p2);
    });

    Route::get('cart',function (){
        Cart::add('293ak', 'Product 2', 1, 9.99);
    });
    Route::get('xoa', function(){
//        return Cart::tax();
//        return Cart::count();
//        echo '<pre>';
        return Cart::destroy();
//        foreach (Cart::content() as $item){
//            echo $item->model->image;
//        };
//
//            return array_slice(Cart::content()->toArray(),0,2);
    });
    Route::get('updatcard/{id}', function($id){
       Cart::update($id, 5);

    });
    Route::get('/test123', function(){
        $products =  \App\Models\Admin\Product_cat::find(10)
            ->product
            ->where('id', 44)
            ->first();

           // ->product = ->product()->get() return ve collection
            // ->product() query builder
        // cau nay se query ra tât cả  product  thuộc product_cat id =10 rồi where bằng collection ra product id = 44

        return $products =  \App\Models\Admin\Product_cat::find(10)
            ->product()
            ->where('id', 44)
            ->first();
        // cau nay se query ra chỉ duy nhất product có id=44 thuộc product_cat id =10

        // tóm lại là 1 câu query ra tất và 1 câu qqery ra 1. nên câu thứ 2 sẽ nhanh hơn.
//        product và product() khac gi nhau ?
        return view('welcome', compact($products));
    });

});