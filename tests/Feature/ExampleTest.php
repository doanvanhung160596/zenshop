<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $array = [
            ['title' => 'test', 'name' => 'test1'],
            ['title' => 'new', 'name' => 'new1'],
        ];

        $kq = $this->filter($array, function($item){
            return $item['title'] == 'new';
        });
//        $kq = $this->map($array, function($item){
//            return 'title:' . $item['title'];
//        });
//         $this->assertEquals(
//             ['title:test', 'title:new'], $kq
//         );
        $this->assertEquals(
            ['title' => 'new', 'name' => 'new1'], $kq
        );
    }

    public function map($array, $func)
    {
        $rs = [];
        foreach($array as $item)
        {
            $rs[] = $func($item);
        }
        return $rs;
    }
    public function filter($array, $func)
    {
        $rs = [];
        foreach($array as $item)
        {
            if($func($item))
            {
                $rs = $item;
            }
        }
        return $rs;
    }
}
