@extends('display.index')
@section('content')
    <div id="wrap">
        <!-- Top bar -->
        <!-- Header -->
        <!-- Content -->
        <div id="content">
            <!-- Linking -->
            <div class="linking">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{route('home')}}">@lang('display_lang.home')</a></li>
                        <li><a href="{{route('post.category',$check_category->slug)}}">{{$check_category->title}}</a></li>
                        <li><a href="{{route('post.display',[$check_category->slug,$check_parent->slug])}}">{{$check_parent->title}}</a></li>
                        <li><a href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$post->slug])}}">{{$post->title}}</a></li>
                    </ol>
                </div>
            </div>

            <!-- Blog -->
            <section class="blog-single padding-top-30 padding-bottom-60">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">

                            <!-- Blog Post -->
                            <div class="blog-post">
                                <h6>{{$post->title}}</h6>
                                {!! $post->content !!}
                                <!-- Comments -->
                                    <div class="comments">
                                        <div class="fb-comments" data-href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$post->slug])}}" data-numposts="15"></div>
                                        <div id="fb-root"></div>
                                    </div>
                                <!-- ADD comments -->
                            </div>

                        </div>

                        <!-- Side Bar -->
                        <div class="col-md-3">
                            <div class="shop-side-bar">

                                <!-- Search -->
                                <div class="search">
                                    <form action="{{route('search.post')}}" method="GET">
                                        <label>
                                            <input type="text" value="@if(isset($search)) {{$search}} @endif" name="search" placeholder="Tìm kiếm ...">
                                        </label>
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <!-- Categories -->
                                {{--<h6>Danh Mục Bài Viết</h6>--}}
                                {{--<div class="checkbox checkbox-primary">--}}
                                    {{--<ul>--}}
                                        {{--@foreach($category_category as $item_category_category)--}}
                                            {{--<li>--}}
                                                {{--<input id="cate1" class="styled" type="checkbox" >--}}
                                                {{--<a href="{{route('post.category',$item_category_category->slug)}}"> {{$item_category_category->title}} </a>--}}
                                            {{--</li>--}}
                                        {{--@endforeach()--}}
                                    {{--</ul>--}}
                                {{--</div>--}}

                                <!-- Recent Posts -->
                                <h6>Bài viết tương tự</h6>
                                <div class="recent-post">
                                    <!-- Recent Posts -->
                                    @foreach($similar_posts as $item_similar_posts)
                                        <div class="media">
                                            <div class="media-left"> <a href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$item_similar_posts->slug])}}"><img class="img-responsive" src="{{asset($item_similar_posts->image)}}" alt=""> </a> </div>
                                            <div class="media-body"> <a href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$item_similar_posts->slug])}}">{{$item_similar_posts->title}}</a> <span><i class="fa fa-bookmark-o"></i> {{$item_similar_posts->created_at}}</span><span style="border: none;"> <i class="fa fa-eye"></i> {{$item_similar_posts->viewer}} </span> </div>
                                        </div>
                                    @endforeach
                                </div>
                                <h6>Bài viết đã xem</h6>
                                <div class="recent-post">
                                    <!-- Recent Posts -->
                                    @foreach($recent_post as $item_recent_post)
                                        <div class="media">
                                            <div class="media-left"> <a href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$item_recent_post->slug])}}"><img class="img-responsive" src="{{asset($item_recent_post->image)}}" alt=""> </a> </div>
                                            <div class="media-body"> <a href="{{route('post.display.show',[$check_category->slug,$check_parent->slug,$item_recent_post->slug])}}">{{$item_recent_post->title}}</a> <span><i class="fa fa-bookmark-o"></i> {{$item_recent_post->created_at}}</span><span style="border: none;"> <i class="fa fa-eye"></i> {{$item_recent_post->viewer}} </span> </div>
                                        </div>
                                    @endforeach
                                </div>
                                <!-- Quote of the Day -->
                                <h6>@lang('display_lang.quote')</h6>
                                <div class="quote-day"> <i class="fa fa-quote-left"></i>
                                    <p>
                                        {{$quote->title}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Clients img -->
        </div>
        <!-- End Content -->
    </div>
@endsection