<!-- Main Tabs Sec -->
<section class="main-tabs-sec padding-bottom-60 padding-top-60">
    <div class="container">
        <ul class="nav margin-bottom-40" role="tablist">
            <li role="presentation" class="active"><a href="#tv-au" aria-controls="featur" role="tab" data-toggle="tab"> <i class="flaticon-computer"></i> @lang('display_lang.tv_audios') <span>236 item(s)</span></a></li>
            <li role="presentation"><a href="#smart" aria-controls="special" role="tab" data-toggle="tab"><i class="flaticon-smartphone"></i>@lang('display_lang.smart_phone') <span>150 item(s)</span></a></li>
            <li role="presentation"><a href="#deks-lap" aria-controls="on-sal" role="tab" data-toggle="tab"><i class="flaticon-laptop"></i>@lang('display_lang.desk_laptop') <span>268 item(s)</span></a></li>
            <li role="presentation"><a href="#game-com" aria-controls="special" role="tab" data-toggle="tab"><i class="flaticon-gamepad-1"></i>@lang('display_lang.game_console') <span>79 item(s)</span></a></li>
            <li role="presentation"><a href="#watches" aria-controls="on-sal" role="tab" data-toggle="tab"><i class="flaticon-computer-1"></i>@lang('display_lang.watches') <span>105 item(s)</span></a></li>
            <li role="presentation"><a href="#access" aria-controls="on-sal" role="tab" data-toggle="tab"><i class="flaticon-keyboard"></i>@lang('display_lang.accessories') <span>816 item(s)</span></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- TV & Audios -->
            <div role="tabpanel" class="tab-pane active fade in" id="tv-au">

                <!-- Items -->
                <div class="item-col-5">
                    @foreach($tv_audios as $item_tv_audios)
                    <!-- Product -->
                    <div class="product">
                        <article><a href="{{route('product.display.show',[$item_tv_audios->category->slug,$item_tv_audios->product_cat->slug,$item_tv_audios->slug])}}"><img class="img-responsive" style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"  src="{{asset($item_tv_audios->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_tv_audios->product_discount}}%</span>

                            <!-- Content -->
                            <span class="tag">{{$item_tv_audios->category->title}}</span> <a href="{{route('product.display.show',[$item_tv_audios->category->slug,$item_tv_audios->product_cat->slug,$item_tv_audios->slug])}}" class="tittle">{{substr($item_tv_audios->product_name,0,50) }}</a>
                            <!-- Reviews -->
                            <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_tv_audios->viewer}} @lang('display_lang.view')</span></p>
                            <div class="price">{{number_format($item_tv_audios->price_sale).'đ'}}</div>
                            <br>
                            <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_tv_audios->price).' đ'}}</strike> -{{$item_tv_audios->product_discount}}%</div>
                            <a href="javascript:void(0)" onclick="addtocart({{$item_tv_audios->id}})" id = "item-{{$item_tv_audios->id}}" data_cart="{{$item_tv_audios->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                    </div>
                    @endforeach()
                </div>
            </div>

            <!-- Smartphones -->
            <div role="tabpanel" class="tab-pane fade" id="smart">
                <!-- Items -->
                <div class="item-col-5">

                @foreach($smart_phone as $item_smart_phone)
                    <!-- Product -->
                        <div class="product">
                            <article><a href="{{route('product.display.show',[$item_smart_phone->category->slug,$item_smart_phone->product_cat->slug,$item_smart_phone->slug])}}"><img class="img-responsive" style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"   src="{{asset($item_smart_phone->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_smart_phone->product_discount}}%</span>

                                <!-- Content -->
                                <span class="tag">{{$item_smart_phone->category->title}}</span> <a href="{{route('product.display.show',[$item_smart_phone->category->slug,$item_smart_phone->product_cat->slug,$item_smart_phone->slug])}}" class="tittle">{{substr($item_smart_phone->product_name,0,50) }}</a>
                                <!-- Reviews -->
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_smart_phone->viewer}} @lang('display_lang.view')</span></p>
                                <div class="price">{{number_format($item_smart_phone->price_sale).'đ'}}</div>
                                <br>
                                <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_smart_phone->price).' đ'}}</strike> -{{$item_smart_phone->product_discount}}%</div>
                                <a href="javascript:void(0)" onclick="addtocart({{$item_smart_phone->id}})" id = "item-{{$item_smart_phone->id}}" data_cart="{{$item_smart_phone->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                        </div>
                    @endforeach()

                </div>
            </div>
            <!-- Desk & Laptop -->
            <div role="tabpanel" class="tab-pane fade" id="deks-lap">

                <!-- Items -->
                <div class="item-col-5">

                    <!-- Product -->
                @foreach($desk_laptop as $item_desk_laptop)
                    <!-- Product -->
                        <div class="product">
                            <article><a href="{{route('product.display.show',[$item_desk_laptop->category->slug,$item_desk_laptop->product_cat->slug,$item_desk_laptop->slug])}}"><img class="img-responsive" style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"   src="{{asset($item_desk_laptop->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_desk_laptop->product_discount}}%</span>

                                <!-- Content -->
                                <span class="tag">{{$item_desk_laptop->category->title}}</span> <a href="{{route('product.display.show',[$item_desk_laptop->category->slug,$item_desk_laptop->product_cat->slug,$item_desk_laptop->slug])}}" class="tittle">{{substr($item_desk_laptop->product_name,0,50) }}</a>
                                <!-- Reviews -->
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_desk_laptop->viewer}} @lang('display_lang.view')</span></p>
                                <div class="price">{{number_format($item_desk_laptop->price_sale).'đ'}}</div>
                                <br>
                                <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_desk_laptop->price).' đ'}}</strike> -{{$item_desk_laptop->product_discount}}%</div>
                                <a href="javascript:void(0)" onclick="addtocart({{$item_desk_laptop->id}})" id = "item-{{$item_desk_laptop->id}}" data_cart="{{$item_desk_laptop->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                        </div>
                    @endforeach()

                </div>
            </div>
            <!-- Game Console -->
            <div role="tabpanel" class="tab-pane fade" id="game-com">

                <!-- Items -->
                <div class="item-col-5">

                    <!-- Product -->
                @foreach($game_console as $item_game_console)
                    <!-- Product -->
                        <div class="product">
                            <article><a href="{{route('product.display.show',[$item_game_console->category->slug,$item_game_console->product_cat->slug,$item_game_console->slug])}}"><img style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"   class="img-responsive" src="{{asset($item_game_console->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_game_console->product_discount}}%</span>

                                <!-- Content -->
                                <span class="tag">{{$item_game_console->category->title}}</span> <a href="{{route('product.display.show',[$item_game_console->category->slug,$item_game_console->product_cat->slug,$item_game_console->slug])}}" class="tittle">{{substr($item_game_console->product_name,0,50) }}</a>
                                <!-- Reviews -->
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_game_console->viewer}} @lang('display_lang.view')</span></p>
                                <div class="price">{{number_format($item_game_console->price_sale).'đ'}}</div>
                                <br>
                                <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_game_console->price).' đ'}}</strike> -{{$item_game_console->product_discount}}%</div>
                                <a href="javascript:void(0)" onclick="addtocart({{$item_game_console->id}})" id = "item-{{$item_game_console->id}}" data_cart="{{$item_game_console->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                        </div>
                    @endforeach()

                </div>
            </div>
            <!-- Watches -->
            <div role="tabpanel" class="tab-pane fade" id="watches">

                <!-- Items -->
                <div class="item-col-5">

                    <!-- Product -->
                @foreach($watches as $item_watches)
                    <!-- Product -->
                        <div class="product">
                            <article><a href="{{route('product.display.show',[$item_watches->category->slug,$item_watches->product_cat->slug,$item_watches->slug])}}"><img style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"  class="img-responsive" src="{{asset($item_watches->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_watches->product_discount}}%</span>

                                <!-- Content -->
                                <span class="tag">{{$item_watches->category->title}}</span> <a href="{{route('product.display.show',[$item_watches->category->slug,$item_watches->product_cat->slug,$item_watches->slug])}}" class="tittle">{{substr($item_watches->product_name,0,50) }}</a>
                                <!-- Reviews -->
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_desk_laptop->viewer}} @lang('display_lang.view')</span></p>
                                <div class="price">{{number_format($item_watches->price_sale).'đ'}}</div>
                                <br>
                                <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_watches->price).' đ'}}</strike> -{{$item_watches->product_discount}}%</div>
                                <a href="javascript:void(0)" onclick="addtocart({{$item_watches->id}})" id = "item-{{$item_watches->id}}" data_cart="{{$item_watches->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                        </div>
                    @endforeach()

                </div>
            </div>
            <!-- Accessories -->
            <div role="tabpanel" class="tab-pane fade" id="access">

                <!-- Items -->
                <div class="item-col-5">

                    <!-- Product -->
                @foreach($accessories as $item_accessories)
                    <!-- Product -->
                        <div class="product">
                            <article><a href="{{route('product.display.show',[$item_accessories->category->slug,$item_accessories->product_cat->slug,$item_accessories->slug])}}"><img class="img-responsive" style="max-width:184.03px; min-width: 184.03px; min-height:184.03px; max-height:184.03px;"  src="{{asset($item_accessories->image)}}" alt="" ></a> <span class="sale-tag">-{{$item_accessories->product_discount}}%</span>

                                <!-- Content -->
                                <span class="tag">{{$item_accessories->category->title}}</span> <a href="{{route('product.display.show',[$item_accessories->category->slug,$item_accessories->product_cat->slug,$item_accessories->slug])}}" class="tittle">{{substr($item_accessories->product_name,0,50) }}</a>
                                <!-- Reviews -->
                                <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">{{$item_accessories->viewer}} @lang('display_lang.view')</span></p>
                                <div class="price">{{number_format($item_accessories->price_sale).'đ'}}</div>
                                <br>
                                <div class="price" style="font-size: 13px; color: #00000061;"><strike>{{number_format($item_accessories->price).' đ'}}</strike> -{{$item_accessories->product_discount}}%</div>
                                <a href="javascript:void(0)" onclick="addtocart({{$item_accessories->id}})" id = "item-{{$item_accessories->id}}" data_cart="{{$item_game_console->id}}" class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                        </div>
                    @endforeach()

                </div>
            </div>
        </div>
    </div>
</section>