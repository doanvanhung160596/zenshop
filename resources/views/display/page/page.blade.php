@extends('display.index')
@section('content')
    <div id="wrap">

        <!-- Top bar -->

        <!-- Header -->

        <!-- Content -->
        <div id="content">
            <!-- Linking -->
            <div class="linking">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="{{route('home')}}">@lang('display_lang.home')</a></li>
                        <li><a href="{{route('page',"$page->slug")}}">{{$page->title}}</a></li>
                    </ol>
                </div>
            </div>
            <!-- Blog -->
            <section class="blog-single padding-top-30 padding-bottom-60">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- Blog Post -->
                            <div class="blog-post">
                                <h6>{{$page->title}}</h6>
                            {!! $page->content_page !!}
                            <!-- Comments -->
                                <div class="comments">
                                    <div class="fb-comments" data-href="{{route('page',"$page->slug")}}" data-numposts="10"></div>
                                    <div id="fb-root"></div>
                                </div>
                                <!-- ADD comments -->
                            </div>

                        </div>

                        <!-- Side Bar -->
                        {{--<div class="col-md-3">--}}
                            {{--<div class="shop-side-bar">--}}
                                {{--<!-- Quote of the Day -->--}}
                                {{--<h6>@lang('display_lang.quote')</h6>--}}
                                {{--<div class="quote-day"> <i class="fa fa-quote-left"></i>--}}
                                    {{--<p>--}}
                                        {{--{{$quote->title}}--}}
                                    {{--</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>
                </div>
            </section>

            <!-- Clients img -->
        </div>
        <!-- End Content -->

    </div>
@endsection