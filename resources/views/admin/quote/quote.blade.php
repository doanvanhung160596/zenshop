@extends('admin.index')

@section('content')
    <div id="main-content-wp" class="list-post-page">
        <div class="section" id="title-page">
            <div class="clearfix">
                <a href="{{route('quote.create')}}" title="Thêm Mới" id="add-new" class="fl-left">Thêm Mới</a>
                <h3 id="index" class="fl-left">Danh sách trang</h3>
            </div>
        </div>
        <div class="wrap clearfix">
            @include('admin.siderbar')
            <div id="content" class="fl-right">
                <div class="section" id="detail-page">
                    <div class="section-detail">
                        <div class="filter-wp clearfix">
                            <ul class="post-status fl-left">
                                    {{--<li class="all"><a href="">Tìm thấy <span class="count"> ({{$pages_count}})</span></a> |</li>--}}
                                    {{--<li class="all"><a href="{{route('page.list')}}">Tất cả <span class="count"> ({{$pages_all}})</span></a> </li>--}}
                            </ul>
                            {{--<form method="GET" action="{{route('page.search')}}" class="form-s fl-right">--}}
                                {{--<input type="text" name="value" value="{{old('value')}}" id="s">--}}
                                {{--<input type="submit" name="search" value="Tìm kiếm">--}}
                            {{--</form>--}}
                        </div>
                        <form action="{{route('quote.status')}}" method="post" class="form-actions">
                            @csrf
                            <div class="actions">
                                <select name="actions">
                                    <option value="1">Đăng</option>
                                    <option value="-1">Chờ</option>
                                    <option value="delete">Xóa</option>
                                </select>
                                <input type="submit">
                            </div>
                            <div class="table-responsive">
                                <table class="table list-table-wp">
                                    <thead>
                                    <tr>
                                        <td><input type="checkbox" name="checkAll" id="checkAll"></td>
                                        <td><span class="thead-text">STT</span></td>
                                        <td><span class="thead-text">Tiêu đề Vi</span></td>
                                        <td><span class="thead-text">Tiêu đề Eng</span></td>
                                        <td><span class="thead-text">Trạng thái</span></td>
                                        <td><span class="thead-text">Người tạo</span></td>
                                        <td><span class="thead-text">Thời gian</span></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $count=0 ?>
                                    @foreach($quote as $item_quote)
                                        <?php $count++ ?>
                                        <tr>
                                            <td><input type="checkbox" name="checkItem[]" value="{{ $item_quote->id }}" class="checkItem"></td>
                                            <td><span class="tbody-text"><h3>{{ $count }}</h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a href="{{route('quote.edit',$item_quote->id)}}" title="{{ $item_quote->title_vi }}">{{ $item_quote->title_vi }}</a>
                                                </div>
                                                <ul class="list-operation fl-right">
                                                    <li><a href="{{route('quote.edit',$item_quote->id)}}" title="Sửa" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text">{{$item_quote->title_en}}</span></td>
                                            <td>
                                                @if ($item_quote->status == '1')
                                                    <span class="tbody-text" style="color: red;">Đã Đăng</span>
                                                @else
                                                    <span class="tbody-text" style="color: #0f9a87">Đang chờ</span>
                                                @endif
                                            </td>
                                            <td><span class="tbody-text">{{$item_quote->user->name}}</span></td>
                                            <td><span class="tbody-text">{{$item_quote->created_at}}</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <thead>
                                    <tr>
                                        <td><input type="checkbox" name="checkAll" id="checkAll"></td>
                                        <td><span class="thead-text">STT</span></td>
                                        <td><span class="thead-text">Tiêu đề Vi</span></td>
                                        <td><span class="thead-text">Tiêu đề Eng</span></td>
                                        <td><span class="thead-text">Trạng thái</span></td>
                                        <td><span class="thead-text">Người tạo</span></td>
                                        <td><span class="thead-text">Thời gian</span></td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="section" id="paging-wp">
                    <div class="section-detail clearfix">
                        {{ $quote->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
    {{--{{dd($errors)}}--}}
    @if (count($errors) > 0)
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <script>
                        $( document ).ready(function() {
                            toastr.error("{{$error}}");
                        });
                    </script>
                @endforeach
            </ul>
        </div>
    @endif
    <script>
        @if(session()->get('success_status'))
        toastr.success( "{{ session()->get('success_status') }}",{timeOut: 5000});
        @endif
    </script>
@endsection