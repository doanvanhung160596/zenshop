@extends('admin.index')
@section('content')

    <div id="main-content-wp" class="add-cat-page">
        <div class="section" id="title-page">
            <div class="clearfix">
                <a href="{{route('quote.create')}}" title="Thêm mới" id="add-new" class="fl-left">Thêm mới</a>
                <h3 id="index" class="fl-left">Thêm trang</h3>
            </div>
        </div>
        <div class="wrap clearfix">
            @include('admin.siderbar')
            <div id="content" class="fl-right">
                <div class="section" id="detail-page">
                    <div class="section-detail">
                        <form method="POST" action="{{route('quote.update',$quote->id)}}">
                            @csrf
                            <label for="title">Tiêu Đề ( Vi)</label>
                            <input type="text" name="title_vi" id="title" value="{{$quote->title_vi }}">
                            <label for="title">Tiêu Đề ( Eng)</label>
                            <input type="text" name="title_en" id="title" value="{{$quote->title_en }}">
                            <button type="submit" id="btn-submit">Cập nhật</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <script>
                        $( document ).ready(function() {
                            toastr.error("{{$error}}");
                        });
                    </script>
                @endforeach
            </ul>
        </div>
    @endif
    <script>
        @if(session()->get('success_update'))
        toastr.success( "{{ session()->get('success_update') }}",{timeOut: 5000});
        @endif
    </script>
@endsection('content')

